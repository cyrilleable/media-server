## Docker : Technologie de virtualisation
https://docs.docker.com/get-started/
https://docs.docker.com/engine/reference/commandline/docker/

Outils de virtualisation au niveau de l'OS et non au niveau du hardware.

    Virtualisation basé sur le matériel (virtualbox, vmware)
    - Récupération d'une partie des ressources de l'host
    - Création d'une machine virtuelle à l'aide de ces ressources
    - Installation de l'iso sur cette machien virtuelle

    Docker:
    - Pull de l'image docker*
    - Création d'un conteneur*
    - Basé sur l'os et ses ressources

## image
A l'instar du paradigme orienté objet, une image
est une classe qui permet de créer plusieurs instances d'OS
ou plusieurs instances de conteneurs.

Une image se crée à l'aide d'un fichier @dockerfile
- Image de base à utiliser (debian, centos..)
- Informations de l'auteur 
- Variables d'environnement
- Fichiers à transférer
- Ensemble des commandes utiles à l'image
- Commandes pendant et après le build

## Conteneur
Une instance autonome d'un os basée sur une image.
Possible de :
- Apporter des infos, de la modifier comme bon vous semble
- Créer une image basée sur le conteneur pour maintenir les évolutions de votre machine

Dockerfile VS Docker-compose

- Dockerfile permet de définir les informations d'une image, qui sera ensuite utilisée pour un conteneur ou un service
https://docs.docker.com/engine/reference/builder/

- Docker-compose est utile pour l'utilisation de plusieurs services comme dans ce cas.
https://docs.docker.com/compose/compose-file/


Quelques notions importantes :

### version de docker-compose
version : "3.9" 

### Ajout des services
#### services:
  chaque service correspond à un conteneur 
  #### jellyfin:
    // image de base du service jellyfin
      image: lscr.io/linuxserver/jellyfin:latest
    // le nom du conteneur 
      container_name: jellyfin
    // règle de redemarrage
      restart: unless-stopped
    // Les variables d'environnement
      environment:
        PUID: 1000
        PGID: 1000
        TZ: Europe/London
        JELLYFIN_PublishedServerUrl: 192.168.0.6
    // Volumes permettant la persistance des données, meme en cas de down des services
      volumes:
       - tvseries:/data/tvshows #volume géré par docker
       - ./movies:/data/movies #volume non géré par docker (stocké dans le dossier en cours)
       - config:/config
    // port exposé pour le service
      ports
    // port hote:port docker
        - "8096:8096" 

// Pour les volumes gérés par docker
#### volumes:
  tvseries:\
  config:
